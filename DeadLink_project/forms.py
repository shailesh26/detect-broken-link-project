from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User

class SignUpForm(forms.Form):
	username = forms.CharField(label="Username")
	password = forms.CharField(label="Password", widget=forms.PasswordInput())
	confirm_password = forms.CharField(label="Confirm Password", widget=forms.PasswordInput())
	email = forms.EmailField(label = "Email")
	first_name = forms.CharField(label="First Name")
	last_name = forms.CharField(label="Last Name")

	def clean_username(self):
		userid = self.cleaned_data['username']
		if User.objects.filter(username=userid).exists():
			raise ValidationError("Username already exists")
		return userid

	def clean_confirm_password(self):
		password = self.cleaned_data.get('password')
		password1 = self.cleaned_data.get('confirm_password')
		if password and password1:
			if password != password1:
				raise forms.ValidationError("password do not match")
			return password1

class LoginForm(forms.Form):
	username= forms.CharField(label="Username")
	password = forms.CharField(label="Password", widget=forms.PasswordInput())

class DeadLinkForm(forms.Form):
	url = forms.URLField(label="Enter URL")
	level = forms.IntegerField(label="Enter Level", min_value=1, max_value=10)
