from django.shortcuts import render
from DeadLink_project.forms import SignUpForm, LoginForm, DeadLinkForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login 
from django.contrib.auth.decorators import login_required
from django.contrib import auth
import urllib2
import requests
from bs4 import BeautifulSoup
from django.http import HttpResponseRedirect, HttpResponse
from lxml import html
from urlparse import urlparse

# Create your views here.
def register(request):
	if request.method == "POST":
		form = SignUpForm(request.POST)
		if form.is_valid():
			user = User.objects.create_user(
				username = form.cleaned_data['username'],
				password = form.cleaned_data['password'],
				email = form.cleaned_data['email'],
				first_name = form.cleaned_data['first_name'],
				last_name = form.cleaned_data['last_name']
				)
			user.save()
			print "Registration success"
			return HttpResponseRedirect('/accounts/login/')
	else:
		form = SignUpForm()
	return render(request, "signup.html", {'form': form})

def login(request):
	if request.method == "POST":
		form = LoginForm(request.POST)
		if form.is_valid():
			username = request.POST['username']
			password = request.POST['password']
			user = authenticate(username=username, password=password)
			if user is not None and user.is_active:
				login(request, user)
				print "Successfully logged in"
				return HttpResponseRedirect('/broken-link')
			else:
				print "Username and Password is Incorrect"
	else:
		form = LoginForm()
	return render(request, "login.html", {'form': form})

@login_required
def broken_link(request):
	all_url = []
	bad_url = []
	link = []
	length = []
	if request.method == "POST":
		form = DeadLinkForm(request.POST)
		if form.is_valid():
			url = form.cleaned_data['url']
			level = form.cleaned_data['level']
			parsed_uri = urlparse(url)
			domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
			print "domain", domain
			try:
				response = urllib2.urlopen(url)
			except urllib2.HTTPError, e:
				return HttpResponse("Something Went Wrong: Error " + str(e.code))
			soup = BeautifulSoup(response)
			for link in soup.find_all('a', href=True):
				if link['href'] == '' or link['href'].startswith('#') or link['href'].startswith('/ ') or link['href'].startswith('javascript'):
					continue
				print "link ====", link['href']
				new_link = link.get('href')
				if 'http://' in new_link or 'https://' in new_link or 'www' in new_link:
					try:
						r = urllib2.urlopen(new_link)
						all_url.append(new_link)
					except urllib2.HTTPError, e:
						bad_url.append(new_link)
				else:
					strip_url = new_link.lstrip('/')
					new_link = domain + strip_url
					try:
						r = urllib2.urlopen(new_link)
						all_url.append(new_link)
					except urllib2.HTTPError, e:
						bad_url.append(new_link)

				length = [len(all_url) + len(bad_url)]
				print "This is broken link function's link: ", new_link
				detect_link(level, new_link)
			return render(request, "results.html", {'dictionary_of_links': dictionary_of_links, 'length': length, 'url': url, 'all_url': all_url, 'bad_url': bad_url, 'level': level})
	else:
		form = DeadLinkForm()
	return render(request, "broken_link.html", {'form': form})


dictionary_of_links = {}
def detect_link(level, new_link):
	link = []
	count_of_url = []
	value_of_new_link = []
	value_of_new_bad_link = []
	levels = []
	redirected_url = urllib2.urlopen(new_link).geturl()
	parsed_uri = urlparse(redirected_url)
	new_link_domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
	print "new link domain", new_link_domain
	if level == 1:
		return 0
	levels.append(level)
	print "this is level: ", levels
	
	if 'http://' in new_link or 'https://' in new_link:
		response = urllib2.urlopen(new_link)
		soup = BeautifulSoup(response)
	else:
		strip_new_link = new_link.lstrip('/')
		new_link = new_link_domain + strip_new_link
		response = urllib2.urlopen(new_link)
		soup = BeautifulSoup(response)
	for link in soup.find_all('a', href=True):
		if link['href'] == '' or link['href'].startswith('#') or link['href'].startswith('//') or link['href'].startswith('javascript'):
			continue	
		new_url_inside_new_link = link.get('href')
		print "get", new_url_inside_new_link
		if new_url_inside_new_link.startswith('http://') or new_url_inside_new_link.startswith('https://') or new_url_inside_new_link.startswith('javascript'):
			try:
				r = urllib2.urlopen(new_url_inside_new_link)
				value_of_new_link.append(new_url_inside_new_link)
			except urllib2.HTTPError, e:
				value_of_new_bad_link.append(new_url_inside_new_link)

		else:
			strip_new_url_inside_new_link = new_url_inside_new_link.lstrip('/')
			new_url_inside_new_link = new_link_domain + strip_new_url_inside_new_link
			print "$$$$$$$$$$$", new_url_inside_new_link
			try:
				r = urllib2.urlopen(new_url_inside_new_link)
				value_of_new_link.append(new_url_inside_new_link)
			except urllib2.HTTPError, e:
				value_of_new_bad_link.append(new_url_inside_new_link)

			print "This link %s is inside the new_url " % (new_url_inside_new_link) 
		
		count_of_url = [len(value_of_new_link) + len(value_of_new_bad_link)]
		print "count of url", count_of_url
		
		dictionary_of_links[new_link] = [levels, count_of_url, value_of_new_link, value_of_new_bad_link]
	return detect_link(level-1, new_url_inside_new_link)


def logout(request):
	auth.logout(request)
	return HttpResponseRedirect('/accounts/login')

def profile(request):
	return HttpResponseRedirect('/broken-link')

	

